package com.example.demo;
//http://codepad.org/LbYHmPWe
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class DemoAjaxController {
	private final String CONNECTION_URL = "jdbc:mysql://mysql:3306/ShippersDB?useSSL=false&serverTimezone=UTC" +
			"&allowPublicKeyRetrieval=true";
	
	@RequestMapping(method=RequestMethod.GET, value="/echo")
	public String getText(@RequestParam String text) {
		return text;
	}
	// http://codepad.org/FQsii03n
	@RequestMapping(method=RequestMethod.GET, value="/EchoInUppercase")
	public String getTextUpper(@RequestParam String text) {
		return text.toUpperCase();
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/customers")
	public String getCustomers(@RequestParam String text) {
		String customers = "";
		Connection cn = null;
		ResultSet rs = null;
		if(text.length() == 0) {
			return "";
		}
		else {
			try{
				Driver d = new com.mysql.cj.jdbc.Driver();
				DriverManager.registerDriver(d);
				cn = DriverManager.getConnection(CONNECTION_URL,"root","c0nygre");
				String sql = "SELECT CompanyName FROM Customers WHERE " +
				"CompanyName LIKE ?";
				PreparedStatement st = cn.prepareStatement(sql);
				st.setString(1, text + "%");
				rs = st.executeQuery();
				// Process the results of the query, one row at a time
				while (rs.next()) { 
					customers += rs.getString(1) + "<br />";
				}
			}
			catch(SQLException ex){
				System.out.println(ex.getMessage());
			}
			finally{
				if(cn != null){
					try{
						if(!cn.isClosed()){
							cn.close();
						}
					}
					catch(SQLException ex){
						System.out.println(ex.getMessage());
					}
				}
			}
		}
		return customers;
	}
}
