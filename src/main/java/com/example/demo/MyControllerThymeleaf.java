package com.example.demo;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

//Logging
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.example.demo.business.Shipper;
import com.example.demo.business.ShipperRepository;

@Controller
public class MyControllerThymeleaf {
	// ****** Logger from the LoggerFactory.getLogger factory method
	// ****** Use Class_Name.class as the parameter
	// ****** levels
	/*
      LOGGER.debug("Debug Message!");
      LOGGER.info("Info Message!");
      LOGGER.warn("Warn Message!");
      LOGGER.error("Error Message!");
      LOGGER.fatal("Fatal Message!");
	 * 
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(MyControllerThymeleaf.class);	
	@Autowired
	ShipperRepository shipperrepo;
	
	@RequestMapping("/")
	String loginhandler() {
		LOGGER.info("Login Page!");
		return "login";
	}
	
	@RequestMapping("/logout")
	String logouthandler(HttpServletRequest req) {
		LOGGER.info("Logged out!");
		if(req.getSession() != null) {
			req.getSession().invalidate();
			return "login";
		}
		else {
			return "login";
		}
	}
	
	
	@RequestMapping("/home")
	String homehandler(HttpServletRequest req) {
		if(req.getSession().getAttribute("loggedIn")==null) {
			return "login";
		}
		else {
			return "index";
		}
	}
	
	
	
	@RequestMapping("/loginValidate")
	String loginhandler(HttpServletRequest req) {
		if(req.getParameter("uname").equals("fred") && 
				req.getParameter("pword").equals("pass")) {
			req.getSession().setAttribute("loggedIn", true);
			return "index";
		}
		else {
			return "login";
		}
	}
	
	
	@RequestMapping("/newshipper")
	String shippernewhandler(HttpServletRequest req) {
		if(req.getSession().getAttribute("loggedIn")==null) {
			return "login";
		}
		else {
			return "addshipper";
		}
	}
	
	@RequestMapping("/new")
	String shipperinsertnewhandler(HttpServletRequest request, Model model) {
		Shipper ashipper = new Shipper(-1,request.getParameter("name"),
				request.getParameter("phone"));
		shipperrepo.newShipper(ashipper);
		model.addAttribute("shippers", shipperrepo.allShippers());
		return "shippers";
	}
	
	@RequestMapping("/shippers")
	String shippershandler(Model model) {
		model.addAttribute("shippers", shipperrepo.allShippers());
		return "shippers";
	}
	
	@RequestMapping("/edit/{id}")
	String shipperupdatehandler(@PathVariable("id") String id,Model model) {
		model.addAttribute("shipper", shipperrepo.getShipper(Integer.parseInt(id)));
		return "edit";
	}
	
	@RequestMapping("/save")
	String shippersavehandler(@ModelAttribute Shipper newShipper, Model model) {
		shipperrepo.saveShipper(newShipper);
		model.addAttribute("shippers", shipperrepo.allShippers());
		return "shippers";
	}
	
	@RequestMapping("/delete/{id}")
	String shipperdeletehandler(@PathVariable("id") String id, Model model) {
		shipperrepo.deleteShipper(Integer.parseInt(id));
		model.addAttribute("shippers", shipperrepo.allShippers());
		LOGGER.warn("Shipper Deleted");
		return "shippers";
	}
	
}
